//
//  Experience.swift
//  HassanAlbaajCV
//
//  Created by Hassan Al-Baaj on 2018-11-06.
//  Copyright © 2018 HassanAlBaaj. All rights reserved.
//

import Foundation

struct Experience {
    var imageName: String
    var titel: String
    var message: String
}
