//
//  ExperienceTableViewCell.swift
//  HassanAlbaajCV
//
//  Created by Hassan Al-Baaj on 2018-11-06.
//  Copyright © 2018 HassanAlBaaj. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    

    @IBOutlet weak var XPImage: UIImageView!
    @IBOutlet weak var XPTitel: UILabel!
    @IBOutlet weak var XPMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
