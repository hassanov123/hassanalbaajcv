//
//  DetailViewController.swift
//  HassanAlbaajCV
//
//  Created by Hassan Al-Baaj on 2018-11-06.
//  Copyright © 2018 HassanAlBaaj. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var XPImage: UIImageView!
    @IBOutlet weak var XPTitel: UILabel!
    @IBOutlet weak var XPMessage: UITextView!
    
    var experience: Experience = Experience(imageName: "", titel: "", message: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        XPImage.image = UIImage(named: experience.imageName)
        XPTitel.text = experience.titel
        XPMessage.text = experience.message
    }
}
